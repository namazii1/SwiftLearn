fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

### unit_test

```sh
[bundle exec] fastlane unit_test
```

Прогон юнит тестов для определенных схем. Пример вызова: fastlane unit_test scheme:Quizi

### code_coverage

```sh
[bundle exec] fastlane code_coverage
```

Расчет тестового покрытия кода для указанных схем. Должно вызываться только после выполнения лейна unit_test. Пример вызова: fastlane code_coverage scheme:Quizi

### archive_project

```sh
[bundle exec] fastlane archive_project
```

Собрать проект с указанной схемой. Пример вызова: fastlane archive_project scheme:"Dev" filename:"dev_1.2.3"

### archive_beta

```sh
[bundle exec] fastlane archive_beta
```

Собрать релизную сборку проекта. Пример вызова: fastlane archive_beta scheme:"Prod" filename:"release_1.2.3"

### distribute_to_firebase

```sh
[bundle exec] fastlane distribute_to_firebase
```

Отправить уже собранный архив ipa в фаербейз с указанным названием файла. Пример вызова: fastlane distribute_to_firebase filename:"dev_1.2.3" scheme:"Dev"

### update_version

```sh
[bundle exec] fastlane update_version
```

Меняет версию проекта на новую с валидацией самой версии. Допускается версия вида 1.56.45 или 1.34.21(12), где 12 - номер билда. Пример вызова: fastlane update_version version:"1.46.45"

### force_update_version

```sh
[bundle exec] fastlane force_update_version
```

Меняет версию проекта на новую без валидации самой версии. Пример вызова: fastlane force_update_version version:"1.46.45_TASK"

### distribute_to_testflight

```sh
[bundle exec] fastlane distribute_to_testflight
```

Отправить уже собранный архив ipa в Testflight с указанным названием файла. Пример вызова: fastlane distribute_to_testflight itunes_username:"email@gg.ru" filename:"release_1.2.3"

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
